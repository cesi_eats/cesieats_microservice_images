import { Router } from 'express';
import imagesRouter from "@routes/images-router";

const apiRouter = Router();


apiRouter.use(imagesRouter.baseRoute, imagesRouter.router);


export default {
    apiRouter,
} as const;
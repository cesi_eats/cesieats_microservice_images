/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
import { BlobServiceClient } from "@azure/storage-blob";
import azure, {services} from 'azure-storage';

import upload from "@services/upload-image";
import imageService from "@services/image-service";


const baseRoute = "/images";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    add: "/",

} as const;

router.post(p.add, upload, (req,res) => {

    if (!req || !req.file) {
        res.send(400);
        return;
    }

    if (!req.file.filename) {
        return res.status(400).send("No file uploaded.");
    }

    imageService.uploadImageToStorage(req.file.filename, (url : string) => {
        res.status(200).send({
            "message": "Image uploaded successfully.",
            "url": url});
    });

});


// Export default
export default {
    router,
    baseRoute
} as const;

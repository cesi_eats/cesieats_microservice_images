import multer from "multer";
import path from "path";
import {BlobServiceClient} from "@azure/storage-blob";
import fs from "fs";

const string = "DefaultEndpointsProtocol=https;AccountName=cesieats;" +
    "AccountKey=MC+tWTjir5Y53ijAZypgon+Ukje4/t+55XOUmZ/" +
    "4TD4GjoliDum0sU/UcMBOIAIynbHJN36Xu0om+AStVUeyFQ==;EndpointSuffix=core.windows.net";
const storageAccountConnectionString = string;
const blobServiceClient = BlobServiceClient
    .fromConnectionString(storageAccountConnectionString);
const containerName = 'photos';
const containerClient = blobServiceClient.getContainerClient(containerName);

// eslint-disable-next-line @typescript-eslint/ban-types
async function uploadImageToStorage(fileName: string, callback : Function) {

    const createContainerResponse = await containerClient.createIfNotExists();

    if (createContainerResponse.succeeded) {
        console.log(`Container ${containerName} created.`);
    }

    const fullFileName = path.join('uploads/' + fileName);
    const blockBlobClient = containerClient.getBlockBlobClient(fileName);
    await blockBlobClient.uploadFile(fullFileName );
    fs.unlink(fullFileName, (err) => {
        if (err) {
            throw err;
        }
    });
    callback(blockBlobClient.url);
}

export default {
    uploadImageToStorage,
} as const;
import multer from 'multer';
import path from "path";

const storage = multer.diskStorage({
    destination: (req: any, file: any, cb: (arg0: null, arg1: string) => void) => {
        cb(null, 'uploads')
    },
    filename: (req: any, file: any, cb: (arg0: null, arg1: string) => void) => {
        cb(null, Date.now() + "--" + file.originalname)
    }
});
const upload = multer({ storage: storage });


export default upload.single('img')


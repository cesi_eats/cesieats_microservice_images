FROM node:16-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

RUN mkdir uploads

COPY . .

EXPOSE 3000

CMD ["npm", "run", "deploy"]
